# VisualSolutionModel-Swift
## [Visual](https://visual-develop.vapor.cloud)

### Solution JSON
```
/// {
///    "squares":[
///    {
///     "x":0,
///     "y":0
///    }
///    ],
///    "circles":[
///    {
///     "x":0,
///     "y":0
///    }
///    ],
///    "lines":[
///    {
///     "ey":0,
///     "ex":0,
///     "sx":0,
///     "sy":0
///    }
///    ],
///    "triangles":[
///    {
///     "ey":0,
///     "sx":0,
///     "ex":0,
///     "my":0,
///     "mx":0,
///     "sy":0
///    }
///    ]
/// }
```