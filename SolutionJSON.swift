import UIKit

/// Line JSON
/// {
///     "ey":0,
///     "ex":0,
///     "sx":0,
///     "sy":0
/// }
struct Line: Codable {
    var sx: Int
    var sy: Int
    var ex: Int
    var ey: Int
}

/// Triangle JSON
/// {
///     "ey":0,
///     "sx":0,
///     "ex":0,
///     "my":0,
///     "mx":0,
///     "sy":0
/// }
struct Triangle: Codable {
    var sx: Int
    var sy: Int
    var mx: Int
    var my: Int
    var ex: Int
    var ey: Int
}


/// Square JSON
/// {
///    "x":0,
///    "y":0
/// }
struct Square: Codable {
    var x: Int
    var y: Int
}

/// Circle JSON
/// {
///    "x":0,
///    "y":0
/// }
/// ]
struct Circle: Codable {
    var x: Int
    var y: Int
}

/// Solution JSON
/// {
///    "squares":[
///    {
///     "x":0,
///     "y":0
///    }
///    ],
///    "circles":[
///    {
///     "x":0,
///     "y":0
///    }
///    ],
///    "lines":[
///    {
///     "ey":0,
///     "ex":0,
///     "sx":0,
///     "sy":0
///    }
///    ],
///    "triangles":[
///    {
///     "ey":0,
///     "sx":0,
///     "ex":0,
///     "my":0,
///     "mx":0,
///     "sy":0
///    }
///    ]
/// }
struct Solution: Codable {
    var lines: [Line]
    var triangles: [Triangle]
    var squares: [Square]
    var circles: [Circle]
    
    func printJSON() {
        if let JSONString = String(data: try! JSONEncoder().encode(self), encoding: String.Encoding.utf8) {
            print(JSONString)
        }
    }
}

// MARK: Examples

let data = Solution(lines: [
    Line(sx: 0, sy: 10, ex: 50, ey: 20)
    ], triangles: [
        Triangle(sx: -10, sy: -10, mx: -5, my: 10, ex: 0, ey: -15)
    ], squares: [
        Square(x: -40, y: 20)
    ], circles: [
        Circle(x: -30, y: -50)
    ])

data.printJSON()



let sineWave = Solution(lines: [],
                        triangles: [],
                        squares: [],
                        circles: stride(from: -20, to: 20, by: 0.1)
                            .map { i in Circle(x: Int(i * 100),
                                               y: Int(sin(i) * 20)) })

sineWave.printJSON()


let randomBoxes = Solution(lines: [],
                           triangles: [],
                           squares: stride(from: 0, to: 50, by: 1)
                            .map { _ in Square(x: Int.random(in: -50 ... 50),
                                               y: Int.random(in: -50 ... 50)) },
                           circles: [])

randomBoxes.printJSON()


let one = Solution(lines: [Line(sx: 0, sy: 0, ex: 0, ey: 0)], triangles: [Triangle(sx: 0, sy: 0, mx: 0, my: 0, ex: 0, ey: 0)], squares: [Square(x: 0, y: 0)], circles: [Circle(x: 0, y: 0)])

one.printJSON()

